# Python bindings for DjVuLibre

This is the RPM spec for packaging the DjVuLibre Python bindings for
Fedora. Basically, you would need this package for any Python-based
applications you're using to be able to open DjVu files... _But_, the
Fedora package manager should take care of that automatically: If you
actually need this package, hopefully it has already been installed for
you.

[`python-djvulibre`](https://jwilk.net/software/python-djvulibre)
provides Python bindings to the DjVuLibre library.

[DjVuLibre](http://djvu.sourceforge.net/)
provides an open-source implementation of the DjVu file format.

[DjVu](http://www.djvu.org/)
(pronounced like "déjà vu") is a file format for documents and images.
